<?php
include('oreilly.php');
/**
 * This is a download of the account page of https://members.oreilly.com/account/emedia/index
 */
$file = 'extract.html';
$oreilly = new \Bairwell\OReilly();
$books = $oreilly->extractFromAccountPage($file, 'print_books');
?>

Select test:<br/>
<ul>
    <li><a href="?test=1">Compares the information extract from the account list to a populated OPMI list</a></li>
    <li><a href="?test=3">Show a basic list of books (populated from the OPMI service)</a></li>
    <li><a href="?test=4">Show a HTML table list of books (populated from the OPMI service)</a></li>
</ul>
<?php
if (TRUE === isset($_REQUEST['test'])) {
    switch ((int)$_REQUEST['test']) {
        case 1:
            compareBookInfo($file);
            break;
        case 2:
            getSimpleListFromAccountPage($file);
            break;
        case 3:
            getHTMLListFromAccountPage($file);
            break;
    }
}
exit();

/**
 * Shows basic information about the book in a list
 * @param Bairwell\OReilly\Book $book The book
 * @param string $lineEnding Which line ending markup we want to use
 * @return null
 */
function showBookData(\Bairwell\OReilly\Book $book, $lineEnding = PHP_EOL)
{
    if (NULL === $book->getCatalogCode()) {
        print 'No book data found' . $lineEnding;
        return;
    }
    print 'Catalog code: ' . $book->getCatalogCode() . $lineEnding;
    print 'URL         : ' . $book->getUrl() . $lineEnding;
    print 'Small Image : ' . $book->getSmallImageUrl() . $lineEnding;
    print 'Medium Image: ' . $book->getMediumImageUrl() . $lineEnding;
    print 'Large Image : ' . $book->getLargeImageUrl() . $lineEnding;
    print 'Title       : ' ;
    if (NULL===$book->getTitle()) {
        print '(not set)'.$lineEnding;
    } else {
        print $book->getTitle().$lineEnding;
    }
    print 'Author      : ';
    if (NULL===$book->getAuthor()) {
        print '(not set)'.$lineEnding;
    } else {
        print $book->getAuthor().$lineEnding;
    }
    print 'Edition     : ';
    if (NULL===$book->getEdition()) {
        print '(not set)'.$lineEnding;
    } else {
        print $book->getEdition().$lineEnding;
    }
    print 'Issued      : ';
    if (NULL===$book->getIssued()) {
        print '(not set)'.$lineEnding;
    } else {
        print $book->getIssued().$lineEnding;
    }
    print 'ISBNs       : ';
    $isbns = $book->getIsbns();
    if (FALSE === empty($isbns)) {
        /**
         * @var \Bairwell\OReilly\ISBN $isbn
         */
        foreach ($isbns as $isbn) {
            print $isbn->getIsbn() . ' (' . $isbn->getEditionType() . ') ';
        }
    } else {
        print '(not set)';
    }
    print $lineEnding;
    print 'Description :' ;
    if (NULL===$book->getDescription()) {
        print '(not set)'.$lineEnding;
    } else {
        print $book->getDescription().$lineEnding;
    }
}

/**
 * Compares the information we extract just from the account page to that from the OPMI service
 * @param string $file The local location of the account page
 */
function compareBookInfo($file)
{
    $oreilly = new \Bairwell\OReilly();
    $books = $oreilly->extractFromAccountPage($file, 'print_books');
    $book = $books[0];
    print '<table border="1" cellspacing="0" cellpadding="0">' . PHP_EOL;
    print '<tr><th scope="col">Just from account page</th><th scope="col">From OPMI service</th></tr>' . PHP_EOL;
    print '<tr><td>' . PHP_EOL;
    showBookData($book, '<br />' . PHP_EOL);
    print '</td><td>' . PHP_EOL;
    $book = $oreilly->getDetailsFromOPMI($book);
    showBookData($book, '<br />' . PHP_EOL);
    print '</td></tr>' . PHP_EOL;
    print '</table>' . PHP_EOL;


}


/**
 * Show a simple list of just books we own
 * @param string $file The local location of the account page
 */
function getSimpleListFromAccountPage($file)
{
    $oreilly = new \Bairwell\OReilly();
    $books = $oreilly->extractFromAccountPage($file, 'print_books');
    /**
     * @var \Bairwell\OReilly\Book $book
     */
    foreach ($books as $book) {
        $book = $oreilly->getDetailsFromOPMI($book);
        print $book->getISBNForEdition('BOOK') . ' - ' . $book->gettitle() . ' (' . $book->getAuthor() . ') : ' . $book->getEdition() . '<br>';
    }
}

/**
 * Shows a HTML marked up list of books we own
 * @param $file
 */
function getHTMLListFromAccountPage($file)
{
    print '<table>' . PHP_EOL;
    print '<tr><th scope="col">Cover image</th><th scope="col">ISBN</th><th scope="col">Title</th><th scope="col">Author</th><th scope="col">Description</th></tr>' . PHP_EOL;
    $oreilly = new \Bairwell\OReilly();
    $books = $oreilly->extractFromAccountPage($file, 'print_books');
    /**
     * @var \Bairwell\OReilly\Book $book
     */
    foreach ($books as $book) {
        $book = $oreilly->getDetailsFromOPMI($book);
        print '<tr>' . PHP_EOL;
        print '<td><a href="' . $book->getUrl() . '" style="text-decoration:none;"><img src="' . $book->getMediumImageUrl() . '"></a></td>' . PHP_EOL;
        print '<td>' . $book->getISBNForEdition('BOOK') . '</td>' . PHP_EOL;
        print '<td>' . $book->getTitle() . '</td>' . PHP_EOL;
        print '<td>' . $book->getAuthor() . '</td>' . PHP_EOL;
        /**
         * Try and standardise the descriptions
         */
        $htmlised = htmlspecialchars_decode($book->getDescription());
        $noParagraphs = str_replace(array('<p>', '</p>'), '', $htmlised);
        $ourParagraphs = str_replace(array("\r\n", "\n", "\r"), '<br />' . PHP_EOL, $noParagraphs);
        print '<td>' . $ourParagraphs . '</td>' . PHP_EOL;
        print '</tr>' . PHP_EOL;
    }
}