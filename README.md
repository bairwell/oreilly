OReilly
=======
This is a simple program that I (Richard Chiswell) wrote to be able to easily extract all the information about all the O'Reilly books I've purchased and registered on their website.

Requirements
============
PHP 5.3.1+ (namespaces support)

Example usage
=============
1. Login to www.oreilly.com
2. Go to https://members.oreilly.com/account/emedia/index
3. Save that file (my example is "extract.html")
4. Call index.php in your web browser and give it a try

Copyright
=========
This module was originally written by Richard Chiswell of Bairwell Ltd. Whilst Bairwell Ltd holds the copyright on this work, we have licenced it under the MIT licence.

Bairwell Ltd: [http://www.bairwell.com](http://www.bairwell.com) / Twitter: [http://twitter.com/bairwell](http://twitter.com/bairwell)
Richard Chiswell: [http://blog.rac.me.uk](http://blog.rac.me.uk) / Twitter: [http://twitter.com/rchiswell](http://twitter.com/rchiswell)

Trademarks
==========
"O'Reilly", "O'Reilly & Associates Inc" and the "O'Reilly" logos are trademarks or registered trademarks of [O'Reilly & Associates Inc](http://oreilly.com).

The code is NOT an official product of O'Reilly and we have NO connections whatsoever with O'Reilly apart from being very happy customers of theirs.

Licence
=======
This work is licensed under the MIT License

Copyright (c) 2012 [Bairwell Ltd - http://www.bairwell.com](http://www.bairwell.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.