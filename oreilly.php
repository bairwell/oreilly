<?php
namespace Bairwell {

    /**
     * Handles processing requests on O'Reillys site
     */
    class OReilly
    {

        /**
         * The OReilly OPMI interface URL we should use for a particular book
         *
         * The %1$s is sprintf'd to include the product code or ISBN code there
         *
         * @var string
         */
        private $opmiUrl = 'http://opmi.labs.oreilly.com/product/%1$s';

        /**
         * Gets details of a book based on the catalog code
         *
         * This is probably best re-written using a RDF parser: but the regular expressions currently do the job with minimal code
         *
         * @param \Bairwell\OReilly\Book $book The book we are looking for
         * @return \Bairwell\OReilly\Book The populated data
         */
        public function getDetailsFromOPMI(\Bairwell\OReilly\Book $book)
        {
            $url = sprintf($this->opmiUrl, $book->getCatalogCode());
            $opts = array(
                'http' => array('method' => 'GET')
            );
            $context = stream_context_create($opts);

            $contents = file_get_contents($url, false, $context);

            /**
             * Which fields are we after from the RDF and which "Book" function should they be mapped to
             */
            $mapping = array(
                'om:customerTitle' => 'setTitle',
                'dc:description' => 'setDescription',
                'dc:issued' => 'setIssued',
                'om:byline' => 'setAuthor',
                'om:editionLabel' => 'setEdition'
            );
            foreach ($mapping as $element => $function) {
                $matches = array();
                if (1 === preg_match('/\<' . $element . ' [^>]+>([^<]+)/m', $contents, $matches)) {
                    call_user_func(array($book, $function), $matches[1]);
                }
            }
            /**
             * Now try and get the ISBNs and which format that ISBN relates to
             */
            $isbns = array();
            $isbnQuickCheck = array();
            $matches = array();
            $count = preg_match_all('#<dc:hasFormat rdf:resource="urn:x-domain:oreilly.com:product:(\d{13}).([A-z]*)" xmlns:dc="http://purl.org/dc/terms/"/>#m', $contents, $matches, PREG_SET_ORDER);
            if ($count !== FALSE && $count !== 0) {
                foreach ($matches as $match) {
                    if (FALSE === in_array($match[2], $isbnQuickCheck)) {
                        $isbn = new \Bairwell\OReilly\ISBN();
                        $isbn->setIsbn($match[1])->setEditionType($match[2]);
                        $isbns[] = $isbn;
                        $isbnQuickCheck[] = $match[2];
                    }
                }
            }
            if (FALSE === empty($isbns)) {
                $book->setIsbns($isbns);
            }
            return $book;
        }


        /**
         * Extract details of products you own from a your account page
         *
         * @param string $file The file holding a copy of https://members.oreilly.com/account/emedia/index
         * @param string $productType The type of product want to extact
         * @return array
         */
        function extractFromAccountPage($file, $productType)
        {
            if (FALSE === in_array($productType, array('ebooks', 'print_books', 'videos', 'dvds'))) {
                die('Invalid booktype');
            }
            $books = array();
            $doc = new \DOMDocument();
            @$doc->loadHTMLFile($file);
            $xpath = new \DomXPath($doc);
            /**
             * Look for HTML markup in the format <section id="XXX"><ol><li>
             */
            $elements = $xpath->query('//section[@id="' . $productType . '"]/ol/li');
            if (FALSE === is_null($elements)) {
                /**
                 * @var \DOMNodeList $elements
                 * @var \DOMElement $element
                 */
                foreach ($elements as $element) {
                    /**
                     * O'Reilly have helpfully included HTML5 marked up elements which we can parse and extract data from.
                     * Note the IDs are not actually guaranteed to be ISBNs, but are O'Reilly's own product id code
                     */
                    if (TRUE === $element->hasAttribute('data-sort-key-title') && TRUE === $element->hasAttribute('id')) {
                        $title = $element->getAttribute('data-sort-key-title');
                        $catalog_code = $element->getAttribute('id');
                        if (1 === preg_match('/^\d{13}$/', $catalog_code)) {
                            $book = new \Bairwell\OReilly\Book();
                            $book->setCatalogCode($catalog_code)
                                ->setTitle($title);

                            /**
                             * Let's see if we have a h4 set. If so, that'll make a much better title
                             * @var \DOMNodelist $h4
                             */
                            $h4 = $element->getElementsByTagName('h4');
                            /**
                             * @var \DOMElement $possibleTitle
                             */
                            $possibleTitle = $h4->item(0);
                            if (NULL !== $possibleTitle) {
                                $book->setTitle($possibleTitle->nodeValue);
                            }
                            /**
                             * Let's see if we can find an author listed in the paragraphs
                             * @var \DOMNodeList $paragraphs
                             */
                            $paragraphs = $element->getElementsByTagName('p');
                            foreach ($paragraphs as $paragraph) {
                                $value = $paragraph->nodeValue;
                                $matches = array();
                                if (1 === preg_match('/^(By |Edited By )(.*)$/', $value, $matches)) {
                                    $book->setAuthor($matches[2]);
                                }
                            }
                            $books[] = $book;
                        }
                    }
                }
            }
            return $books;
        }
    }
}

/**
 * Our subclass which holds books and ISBNs specific to the OReilly system
 */
namespace Bairwell\OReilly {
    /**
     * A "book"
     */
    class Book
    {

        /**
         * The URL of the O'Reilly website book page
         *
         * The %1$s is sprintf'd to include the product code or ISBN code there
         *
         * @var string
         */
        private $bookUrl = 'http://shop.oreilly.com/product/%1$s.do';

        /**
         * The URL of small images
         *
         * The %1$s is sprintf'd to include the product code or ISBN code there
         *
         * @var string
         */
        private $smallImagesUrl = 'http://akamaicovers.oreilly.com/images/%1$s/sm.gif';

        /**
         * The URL of medium images
         *
         * http://covers.oreilly.com/images/%1$s/bkt.gif is an alternative
         *
         * The %1$s is sprintf'd to include the product code or ISBN code there
         *
         * @var string
         */
        private $mediumImagesUrl = 'http://akamaicovers.oreilly.com/images/%1$s/bkt.gif';

        /**
         * The URL of large images
         *
         * The %1$s is sprintf'd to include the product code or ISBN code there
         *
         * @var string
         */
        private $largeImagesUrl = 'http://akamaicovers.oreilly.com/images/%1$s/cat.gif';

        /**
         * O'Reilly's catalog codes are very similar to ISBNs/EAN-13s (in that they are 13 digits long)
         * and in some cases they are the same as the book's ISBN: but not necessarily.
         *
         * @var string
         */
        private $catalog_code;

        /**
         * @var array A list of matching ISBNs for this edition
         */
        private $isbn;

        /**
         * @var string The author's name
         */
        private $author;

        /**
         * @var string The book's title
         */
        private $title;

        /**
         * Holds a HTML escaped description of the book
         * @var string A brief description of the book
         */
        private $description;

        /**
         * @var string The revision edition
         */
        private $edition;

        /**
         * @var string The issue date
         */
        private $issued;

        /**
         * Gets the URL of the book
         * @return string
         */
        public function getUrl()
        {
            return sprintf($this->bookUrl, $this->getCatalogCode());
        }

        /**
         * Gets the URL of the small cover image
         * @return string
         */
        public function getSmallImageUrl()
        {
            return sprintf($this->smallImagesUrl, $this->getCatalogCode());
        }

        /**
         * Gets the URL of the medium cover image
         * @return string
         */
        public function getMediumImageUrl()
        {
            return sprintf($this->mediumImagesUrl, $this->getCatalogCode());
        }

        /**
         * Gets the URL of the large cover image
         * @return string
         */
        public function getLargeImageUrl()
        {
            return sprintf($this->largeImagesUrl, $this->getCatalogCode());
        }

        /**
         * Gets an ISBN for a specified edition from a book
         * @param string $edition Which edition we are looking for (common ones are "BOOK","MOBI","PDF")
         * @return string|null The ISBN or NULL if not found
         */
        public function getISBNForEdition($edition)
        {
            /**
             * @var array $isbns
             */
            $isbns = $this->getIsbns();
            $toUpper = mb_strtoupper($edition);
            /**
             * @var ISBN $isbn
             */
            foreach ($isbns as $isbn) {
                if ($toUpper === $isbn->getEditionType()) {
                    return $isbn->getIsbn();
                }
            }
            return NULL;
        }

        /**
         * Sets author in a fluent manner
         *
         * @param string $author
         * @return Book
         */
        public function setAuthor($author)
        {
            $this->author = $author;
            return $this;
        }

        /**
         * @return string
         */
        public function getAuthor()
        {
            return $this->author;
        }

        /**
         * Sets catalog_code in a fluent manner
         *
         * @param string $catalog_code
         * @return Book
         */
        public function setCatalogCode($catalog_code)
        {
            $this->catalog_code = $catalog_code;
            return $this;
        }

        /**
         * @return string
         */
        public function getCatalogCode()
        {
            return $this->catalog_code;
        }

        /**
         * Sets edition in a fluent manner
         *
         * @param string $edition
         * @return Book
         */
        public function setEdition($edition)
        {
            $this->edition = $edition;
            return $this;
        }

        /**
         * @return string
         */
        public function getEdition()
        {
            return $this->edition;
        }

        /**
         * Sets isbn in a fluent manner
         *
         * @param array $isbn
         * @return Book
         */
        public function setIsbns($isbn)
        {
            $this->isbn = $isbn;
            return $this;
        }

        /**
         * @return array
         */
        public function getIsbns()
        {
            return $this->isbn;
        }

        /**
         * Sets title in a fluent manner
         *
         * @param string $title
         * @return Book
         */
        public function setTitle($title)
        {
            $this->title = $title;
            return $this;
        }

        /**
         * @return string
         */
        public function getTitle()
        {
            return $this->title;
        }

        /**
         * Sets description in a fluent manner
         *
         * @param string $description
         * @return Book
         */
        public function setDescription($description)
        {
            $this->description = $description;
            return $this;
        }

        /**
         * @return string
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * Sets issued in a fluent manner
         *
         * @param string $issued
         * @return Book
         */
        public function setIssued($issued)
        {
            $this->issued = $issued;
            return $this;
        }

        /**
         * @return string
         */
        public function getIssued()
        {
            return $this->issued;
        }

    }

    /**
     * Holds a book ISBN (EAN-13) code and which edition that relates to
     */
    class ISBN
    {

        /**
         * @var string The ISBN code
         */
        private $isbn;

        /**
         * @var string The type of edition (paperback/hardback/ebook etc)
         */
        private $edition_type;

        /**
         * Sets edition_type in a fluent manner
         *
         * @param string $edition_type
         * @return ISBN
         */
        public function setEditionType($edition_type)
        {
            $this->edition_type = $edition_type;
            return $this;
        }

        /**
         * @return string
         */
        public function getEditionType()
        {
            return $this->edition_type;
        }

        /**
         * Sets isbn in a fluent manner
         *
         * @param string $isbn
         * @return ISBN
         */
        public function setIsbn($isbn)
        {
            $this->isbn = $isbn;
            return $this;
        }

        /**
         * @return string
         */
        public function getIsbn()
        {
            return $this->isbn;
        }
    }
}